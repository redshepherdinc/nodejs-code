# Red Shepherd API Tests and Examples

### Installing

To Setup the test on your computer, git clone this repo and run

```
yarn
```

### Running Tests

To run the test

```
yarn start
```
