import { post, postData, postJson } from "../utils/http.js";

export function create_invoice_test() {
  return new Promise(function (resolve, reject) {
    console.info("******* Create Invoice test *******");

    var d = new Date();
    var id = "DEMO." + d.getTime();

    let data = {
      userId: "17",
      app: "DEMO",
      id: id,
      platformId: "APP",
      displayId: id,
      invoiceDate: d.toDateString(),
      name: id,
      invoiceAmount: 1660082,
      paidAmount: 0,
      balanceAmount: 1660082,
      clientId: "kumar@redshepherd.com",
      clientName: "Kumar Ramdurgkar",
      clientEmail: "kumar@redshepherd.com",
      clientPhone: "1234567890",
      invoiceStatus: "New",
      client: {
        id: "kumar@redshepherd.com",
        firstName1: "Kumar",
        lastName1: "Ramdurgkar",
        phone1: "1234567890",
        email1: "kumar@redshepherd.com",
        addressLine1: "123 S State St",
        addressLine2: "",
        city: "Chicago",
        state: "IL",
        country: "",
        zip: "60605",
      },
      items: [
        {
          name: "Item1",
          description: "Item1",
          price: 100,
          priceIsFixed: true,
          priceIsPercent: false,
          priceAllowsDecimal: true,
          quantity: 1,
          quantityIsFixed: true,
          quantityAllowsDecimal: false,
        },
        {
          name: "Item 2",
          description: "Item 2",
          price: 200,
          priceIsFixed: true,
          priceIsPercent: false,
          priceAllowsDecimal: true,
          quantity: 1,
          quantityIsFixed: true,
          quantityAllowsDecimal: false,
        },
        {
          name: "Item 3",
          description: "Item 3",
          price: 300,
          priceIsFixed: true,
          priceIsPercent: false,
          priceAllowsDecimal: true,
          quantity: 1,
          quantityIsFixed: true,
          quantityAllowsDecimal: false,
        },
        {
          name: "Item 4",
          description: "Item 4",
          price: 2200,
          priceIsFixed: true,
          priceIsPercent: false,
          priceAllowsDecimal: true,
          quantity: 12.1,
          quantityIsFixed: true,
          quantityAllowsDecimal: false,
        },
        {
          name: "Item 5",
          description: "Item 5",
          price: 2100,
          priceIsFixed: true,
          priceIsPercent: false,
          priceAllowsDecimal: true,
          quantity: 33,
          quantityIsFixed: true,
          quantityAllowsDecimal: false,
        },
        {
          name: "Item 6",
          description: "Item 6",
          price: 2200,
          priceIsFixed: true,
          priceIsPercent: false,
          priceAllowsDecimal: true,
          quantity: 11,
          quantityIsFixed: true,
          quantityAllowsDecimal: false,
        },
        {
          name: "Item 7",
          description: "Item 7",
          price: 44400,
          priceIsFixed: true,
          priceIsPercent: false,
          priceAllowsDecimal: true,
          quantity: 33.99,
          quantityIsFixed: true,
          quantityAllowsDecimal: false,
        },
        {
          name: "Item 8",
          description: "Item 8",
          price: 1122,
          priceIsFixed: true,
          priceIsPercent: false,
          priceAllowsDecimal: true,
          quantity: 22,
          quantityIsFixed: true,
          quantityAllowsDecimal: false,
        },
        {
          name: "Really long item description",
          description: "Really long item description",
          price: 1122,
          priceIsFixed: true,
          priceIsPercent: false,
          priceAllowsDecimal: true,
          quantity: 1,
          quantityIsFixed: true,
          quantityAllowsDecimal: false,
        },
        {
          name: "Item 10",
          description: "Item 10",
          price: 4400,
          priceIsFixed: true,
          priceIsPercent: false,
          priceAllowsDecimal: true,
          quantity: 1,
          quantityIsFixed: true,
          quantityAllowsDecimal: false,
        },
      ],
      notes: [
        "A finance charge of 1.5% will be made unpaid balances after 30 days",
      ],
    };

    console.log("Create Invoice request >>>");
    console.log(JSON.stringify(data, null, 2));

    postData("https://b001-backend-prod.azurewebsites.net/invoice/create", data)
      .then((res) => {
        console.log("Response >>>");
        console.log(JSON.stringify(res, null, 2));
        resolve();
      })
      .catch((err) => {
        console.error("Error >>>", err);
        reject();
      });
  });
}

export function get_invoice_by_id_test() {
  return new Promise(function (resolve, reject) {
    console.info("******* Get Invoice By Id test *******");

    console.log(
      "Request POST>>> https://b001-backend-prod.azurewebsites.net/invoice/find/DEMO.1584943459618"
    );

    post(
      "https://b001-backend-prod.azurewebsites.net/invoice/find/DEMO.1584943459618"
    )
      .then((res) => {
        console.log("Response >>>");
        console.log(JSON.stringify(res, null, 2));
        resolve();
      })
      .catch((err) => {
        console.error("Error >>>", err);
        reject();
      });
  });
}

export function email_invoice_test() {
  return new Promise(function (resolve, reject) {
    console.info("******* Email Invoice test *******");

    let data = {
      from: "donotreply@redshepherd.com",
      to: "shri.bhartia@gmail.com",
      subject: "Your Invoice is Ready",
      url:
        "https://dashboard.redshepherd.com/assets/email/customer_invoice.html",
      tokens: [
        {
          key: "INVOICEID",
          value: "RED.637002144353991618",
        },
        {
          key: "BUSINESSNAME",
          value: "TEST Inc.",
        },
        {
          key: "BUSINESSEMAIL",
          value: "support@redshepherd.com",
        },
      ],
    };

    console.log("Email Invoice request >>>");
    console.log(JSON.stringify(data, null, 2));

    postData("https://rsservices.azurewebsites.net/mail", data)
      .then((res) => {
        console.log("Response >>>");
        console.log(JSON.stringify(res, null, 2));
        resolve();
      })
      .catch((err) => {
        console.error("Error >>>", err);
        reject();
      });
  });
}

export function sms_invoice_test() {
  return new Promise(function (resolve, reject) {
    console.info("******* SMS Invoice test *******");

    let data = {
      phone: "5109442407",
      message:
        "Your invoice from Test Inc: https://dashboard.redshepherd.com/invoice?DEMO.1586143705596",
    };

    console.log("SMS Invoice request >>>");
    console.log(JSON.stringify(data, null, 2));

    postData("https://rsservices.azurewebsites.net/sms", data)
      .then((res) => {
        console.log("Response >>>");
        console.log(JSON.stringify(res, null, 2));
        resolve();
      })
      .catch((err) => {
        console.error("Error >>>", err);
        reject();
      });
  });
}

export function search_invoice_new_test() {
  return new Promise(function (resolve, reject) {
    console.info("******* Search New Invoice test *******");

    console.log(
      "Search Invoice request >>>",
      "https://b001-backend-prod.azurewebsites.net/app/invoice/find/DEMO/New"
    );

    postJson(
      "https://b001-backend-prod.azurewebsites.net/app/invoice/find/DEMO/New"
    )
      .then((res) => {
        console.log("Response >>>");
        console.log(JSON.stringify(res, null, 2));
        resolve();
      })
      .catch((err) => {
        console.error("Error >>>", err);
        reject();
      });
  });
}

export function search_invoice_paid_test() {
  return new Promise(function (resolve, reject) {
    console.info("******* Search Paid Invoice test *******");

    console.log(
      "Search Invoice request >>>",
      "https://b001-backend-prod.azurewebsites.net/app/invoice/find/DEMO/Paid"
    );

    postJson(
      "https://b001-backend-prod.azurewebsites.net/app/invoice/find/DEMO/Paid"
    )
      .then((res) => {
        console.log("Response >>>");
        console.log(JSON.stringify(res, null, 2));
        resolve();
      })
      .catch((err) => {
        console.error("Error >>>", err);
        reject();
      });
  });
}

export function search_invoice_all_test() {
  return new Promise(function (resolve, reject) {
    console.info("******* Search Invoice test *******");

    console.log(
      "Search Invoice request >>>",
      "https://b001-backend-prod.azurewebsites.net/app/invoice/find/DEMO"
    );

    postJson(
      "https://b001-backend-prod.azurewebsites.net/app/invoice/find/DEMO"
    )
      .then((res) => {
        console.log("Response >>>");
        console.log(JSON.stringify(res, null, 2));
        resolve();
      })
      .catch((err) => {
        console.error("Error >>>", err);
        reject();
      });
  });
}
