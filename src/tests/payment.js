import { postData } from "../utils/http.js";

import * as RedPay from "../utils/redpay.js";

/*** REPLACE app, url and key with your PROD keys and use a valid account ***/
const app = "DEMO";
const url = "https://redpaystable.azurewebsites.net/";
const key =
  "MIIBojANBgkqhkiG9w0BAQEFAAOCAY8AMIIBigKCAYEAtsQxNp3vmKVNYIxfWSi0LIRgCnPaMn0MUNouxgrs4zmg4cnvSeQ3I8YP03YbpXuWA80RvOw/nWErYAKomniJw8Y+xexMfBQ5sgJgewn3ZnRPNM9Y4Z62gwfIlsrs7Bwvpz9uUtLgeQLl1ffNaumnu1IBrqRps0EZ1QyDuu41UckTyo31C40Wez6IbeMfZeusrmPlIWqyBacdviJ5zHCA3zHNq86QMnB8HOP1U81HOSs6GTTelhD7lCoJ+fHKHxcz0MDr37fNpKpC57B0/20wBXFp9tlVtSkHcIty1lyNk2/HDH8knCdqkZk+fCvWgGwdex41x8/rM+LKC13c5J/yG6Gb2PnKhwNk4lvvnz73YAdqTUJ7qNrdtWVnOTWfbMBiNlpBCVqt8xY8UK6u83AVWrWXse0xe2Pn/kRqlXmxWT0mGEoCavjvZ9lQUL7LXAXZ1dff9r+oFUZo6xDQ3ER/OTIKa4jpvaI9S/J1drsrI1f9kkMWFwEh48dCPYplGSxzAgMBAAE=";

const redpayconfig = new RedPay.Config(app, key, url);
const redpay = new RedPay.RedPayCardProcessor(redpayconfig);

export function demo_creditcard_payment_test() {
  return new Promise(function (resolve, reject) {
    console.info("******* Credit Card Payment test *******");

    let req = {
      account: "4111111111111111",
      amount: 2000,
      expmmyyyy: "122020",
      cvv: "123",
      cardHolderName: "John Smith",
      avsZip: "10001",
    };

    console.log("Credit Card Payment Request >>>");
    console.log(JSON.stringify(req, null, 2));

    redpay.Charge(req).then(
      function (res) {
        console.log("Credit Card Payment Response >>>");
        console.log(JSON.stringify(res, null, 2));
        resolve();
      },
      function (err) {
        console.error(JSON.stringify(err, null, 2));
        reject();
      }
    );
  });
}

export function demo_ach_payment_test() {
  return new Promise(function (resolve, reject) {
    console.info("******* New ACH Payment test *******");

    let req = {
      account: "1234567890",
      accountType: "C",
      routing: "121122676",
      amount: 2100,
      cardHolderName: "John Smith",
    };

    console.log("ACH Payment Request >>>");
    console.log(JSON.stringify(req, null, 2));

    redpay.Charge(req).then(
      function (res) {
        console.log("ACH Payment Response >>>");
        console.log(JSON.stringify(res, null, 2));
        resolve();
      },
      function (err) {
        console.error(JSON.stringify(err, null, 2));
        reject();
      }
    );
  });
}

export function get_payments_by_date_range_test() {
  return new Promise(function (resolve, reject) {
    console.info("******* Payments by Date Range test *******");

    let req = {
      app: "DEMO",
      startdate: "2020-02-16T06:00:00.000Z", // Replace startdate with starting date range
      enddate: null, // Replace enddate with ending date range
      responseCode: null,
      displayPaymentCost: "Y",
    };

    console.log("Request >>>");
    console.log(JSON.stringify(req, null, 2));

    postData("https://reddash.azurewebsites.net/transaction/find", req)
      .then((res) => {
        console.log("Response >>>");
        console.log(JSON.stringify(res, null, 2));
        resolve();
      })
      .catch((err) => {
        console.error("Error >>>", err);
        reject();
      });
  });
}
