import { postData } from "../utils/http.js";

import JSEncrypt from "node-jsencrypt";
import CryptoJS from "crypto-js";

export function demo_login_test() {
  return new Promise(function (resolve, reject) {
    console.info("******* Demo Login test *******");

    // This is the demo login, replace with the inputs from the login screen
    let data = {
      username: "demo", // TODO: Replace with login from login screen
      password: "demo123?", // TODO: Replace with password from login screen
    };

    console.log("Login request >>>");
    console.log(JSON.stringify(data, null, 2));

    var r = CryptoJS.lib.WordArray.random(16);

    var k =
      "MIIBojANBgkqhkiG9w0BAQEFAAOCAY8AMIIBigKCAYEAvpPsWMeipJMGOQ2yf6Ux53a+PBqpHdOhAF1SHFVOAuDM1Toj6RyC5L1/YwCXUQ025k2WHRJbKDlQOs/l7Ry1om1KVQxEYb4/MCmoTC1/nFLQ7iKysdGzXaaeADsTxXoCKyuNkYOduuWbZDTIVPPf7fNq3vIInxLzI0qt/tgAUeKP+Z52wak9qfd2hwDmcYG4m5H1sJgGBFwLadxqK98Zsq263UL7Ubq996qFvTx/DSgVvc9ODwSdZf2+OT5ozVx4VciS7Em9NahVWr2Qh77l9NxZxUNyj7RmrZDFOAO7kpCgCvJi/hojMt+BeW5QPTO7UUbzUwEAM1w8wdnT2CH/ghVi6wfDeURrILyG77cKBGzaMNcRUbsBj4aKCYmlgkyI5iOe6/Xg0VMER2tChcLkCsRaOcQFOPARvFtv2HKERX1aWVfAQgwRxJHQebX/mnmUpMB2YKl2NvvK1bp/5W0KD54FiGWp6gLf+NzZlN7YzTo7ewAJ03mFqymtk0owzQ81AgMBAAE=";
    var r64 = CryptoJS.enc.Base64.stringify(r);

    var jsencrypt = new JSEncrypt();
    jsencrypt.setPublicKey(k);
    var c = jsencrypt.encrypt(r64).toString();

    var keyreq = {
      rKey: k,
      aKey: c,
    };

    postData("https://reddash.azurewebsites.net/elogin", keyreq)
      .then((ses) => {
        var s = ses.session;
        var a = CryptoJS.lib.WordArray.random(16);
        var a64 = CryptoJS.enc.Base64.stringify(a);

        var p = JSON.stringify(data);

        var b = CryptoJS.enc.Base64.parse(r64);
        var i = CryptoJS.enc.Base64.parse(a64);

        var eb = CryptoJS.AES.encrypt(p, b, {
          iv: i,
          mode: CryptoJS.mode.CBC,
          padding: CryptoJS.pad.Pkcs7,
        });

        var d = eb.toString();

        var packet = {
          session: s,
          data: d,
          ikey: a64,
        };

        postData("https://reddash.azurewebsites.net/elogin", packet)
          .then((res) => {
            console.log("Login response >>>");
            console.log(JSON.stringify(res, null, 2));
            if (res.status !== "A") {
              // User is NOT Active, navigate to Screen that says your account is not active, Please contact Business Admin
              // exit
            } else if (res.twoFactor === "A") {
              // User has 2 Factor Auth set, navigate to Two factor input screen
            } else {
              var roles = res.role ? res.role.split(",") : [];

              // admin role is for Admin Users of a Business
              var admin =
                res.roles !== undefined && res.roles.indexOf("admin") >= 0;
              // gmode role is for Red Shepherd Admin User, this user can access all Business and setup screens
              var gmode =
                res.roles !== undefined && res.roles.indexOf("gmode") >= 0;
              // pmode role is for Processor Admin User, this user can only access Business which they Process transactions for.
              var pmode =
                res.roles !== undefined && res.roles.indexOf("pmode") >= 0;

              // Go to Home screen
            }
            resolve();
          })
          .catch((err) => {
            console.error("Error >>>", err);
            reject();
          });
      })
      .catch((err) => {
        console.error("Error >>>", err);
        reject();
      });
  });
}
