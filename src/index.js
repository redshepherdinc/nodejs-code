import { demo_login_test } from "./tests/login.js";

import {
  demo_creditcard_payment_test,
  demo_ach_payment_test,
  get_payments_by_date_range_test,
} from "./tests/payment.js";

import {
  create_invoice_test,
  get_invoice_by_id_test,
  email_invoice_test,
  sms_invoice_test,
  search_invoice_new_test,
  search_invoice_paid_test,
  search_invoice_all_test,
} from "./tests/invoice.js";

import {
  tokenize_creditcard_test,
  tokenize_ach_test,
} from "./tests/tokenize.js";

Promise.resolve()

  // //#region Login Tests
  // .then((x) => demo_login_test())
  // // //#endregion

  // // //#region Payment Tests
  // .then((x) => demo_creditcard_payment_test())
  // .then((x) => demo_ach_payment_test())
  // .then((x) => get_payments_by_date_range_test())
  // // //#endregion

  // // //#region Invoice Tests
  // .then((x) => create_invoice_test())
  // .then((x) => get_invoice_by_id_test())
  // .then((x) => email_invoice_test())
  // .then((x) => search_invoice_new_test())
  // .then((x) => search_invoice_paid_test())
  // .then((x) => search_invoice_all_test());
  .then((x) => tokenize_ach_test());
//.then((x) => sms_invoice_test());
//#endregion
