/*! RedPay API v2.1.0 | (c) 2017 Red Shepherd Inc. | www.redshepherd.com/license | www.redshepherd.com/docs/js | support@redshepherd.com */
/*! STANDALONE RedPay Api library */
/*! RedPay API v2.5.2 | (c) 2018 Red Shepherd Inc. | www.redshepherd.com/license | support@redshepherd.com */
import CryptoJS from "crypto-js";
import JSEncrypt from "node-jsencrypt";

import { postDataJson } from "./http.js";

export function Config(app, rsaPublicKey, url) {
  this.app = app;
  this.rsaPublicKey = rsaPublicKey;
  this.url = url;
}
export function RedPayCardProcessor(config) {
  var config = config;

  this.Charge = function (redPayRequest) {
    /*
    {
      amount, // default: 0
      account,
      currency, // default: "USD"
      expmmyyyy,
      cvv,
      signatureData,
      cardHolderName,
      method, // default: "CNP"
      authCode,
      retryCount,
      track1Data,
      track2Data,
      avsAddress1,
      avsAddress2,
      avsCity,
      avsZip,
      cardHolderEmail,
      cardHolderPhone,
      ref1,
      ref2,
      ref3,
      ref4,
      ref5
    }
    */
    redPayRequest.action = "A";
    redPayRequest.amount = redPayRequest.amount || 0;
    redPayRequest.currency = redPayRequest.currency || "USD";
    redPayRequest.method = redPayRequest.method || "CNP";
    redPayRequest.productRef = redPayRequest.productRef || "E";

    return SendRequest(redPayRequest, "/ecard");
  };
  this.TokenCharge = function (redPayRequest) {
    /*
    {
      token: 'dsfsdr3234234'
      amount: 1990
    }
    */
    redPayRequest.action = "TA";
    redPayRequest.amount = redPayRequest.amount || 0;
    redPayRequest.currency = redPayRequest.currency || "USD";
    redPayRequest.method = redPayRequest.method || "CNP";
    redPayRequest.productRef = redPayRequest.productRef || "E";

    return SendRequest(redPayRequest, "/ecard");
  };
  this.RecurringCharge = function (redPayRequest) {
    /*
    {
      token,
      amount,
      schedule,
      startDate,
      endDate,
      runNow,
      email,
      avsZip,
      cardHolderName,
      comments
    }
    */
    return SendRequest(redPayRequest, "/eplan/create");
  };
  this.Tokenize = function (redPayRequest) {
    /*
    {
      account, // required
      currency, // default: "USD"
      expmmyyyy, // required
      cvv, // required
      signatureData,
      cardHolderName, // required
      email,
      method, // default: "CNP"
      authCode,
      retryCount,
      track1Data,
      track2Data,
      avsAddress1,
      avsAddress2,
      avsCity,
      avsZip,
      cardHolderEmail,
      cardHolderPhone,
      ref1,
      ref2,
      ref3,
      ref4,
      ref5
    }
    */
    redPayRequest.action = "T";
    redPayRequest.currency = redPayRequest.currency || "USD";
    redPayRequest.method = redPayRequest.method || "CNP";
    redPayRequest.productRef = redPayRequest.productRef || "E";

    return SendRequest(redPayRequest, "/token");
  };
  this.ACH = function (redPayRequest) {
    /*
    {
      amount, // default: 0
      account,
      currency, // default: "USD"
      signatureData,
      cardHolderName,
      authCode,
      retryCount,
      track1Data,
      track2Data,
      avsAddress1,
      avsAddress2,
      avsCity,
      avsZip,
      cardHolderEmail,
      cardHolderPhone,
      ref1,
      ref2,
      ref3,
      ref4,
      ref5
    }
    */
    redPayRequest.action = "A";
    redPayRequest.amount = redPayRequest.amount || 0;
    redPayRequest.currency = redPayRequest.currency || "USD";
    redPayRequest.productRef = redPayRequest.productRef || "E";

    return SendRequest(redPayRequest, "/ach");
  };
  function SendRequest(redPayRequest, route) {
    var randomAesKeyBase64String = Crypto.getBase64String(
      Crypto.generateRandomAesKeyBytes()
    );

    var rsaPublicKey = config.rsaPublicKey;
    var rsaCipherText = RSACryptography.encryptRsa(
      rsaPublicKey,
      randomAesKeyBase64String
    );

    var keyRequest = {
      rsaPublicKey: rsaPublicKey,
      aesKey: rsaCipherText,
    };

    return PostPacket(keyRequest, route).then(function (keyResponse) {
      if (keyResponse == null) return null;

      var sessionId = keyResponse.sessionId;

      var randomIVBase64String = Crypto.getBase64String(
        Crypto.generateRandomIvBytes()
      );
      var aesPlainText = JSON.stringify(redPayRequest);

      var aesCipherText = Crypto.encrypt(
        Crypto.getBytes(randomAesKeyBase64String),
        Crypto.getBytes(randomIVBase64String),
        aesPlainText
      );
      var packet = new Packet(
        sessionId,
        config.app,
        aesCipherText,
        randomIVBase64String
      );

      return PostPacket(packet, route).then(function (packetResponse) {
        if (packetResponse == null) return null;

        var redPayResponse = Crypto.decrypt(
          Crypto.getBytes(randomAesKeyBase64String),
          Crypto.getBytes(packetResponse.iv),
          packetResponse.aesData
        );
        redPayResponse = JSON.parse(redPayResponse);

        return redPayResponse;
      });
    });
  }
  function Packet(sessionId, app, aesData, iv) {
    this.sessionId = sessionId;
    this.app = app;
    this.aesData = aesData;
    this.iv = iv;
  }
  function PostPacket(packet, route) {
    return postDataJson(config.url + route, packet);
  }
}
var Crypto = {
  getBytes: function (base64String) {
    return CryptoJS.enc.Base64.parse(base64String);
  },
  getBase64String: function (byteArray) {
    return CryptoJS.enc.Base64.stringify(byteArray);
  },
  generateRandomIvBytes: function () {
    return CryptoJS.lib.WordArray.random(16);
  },
  generateRandomAesKeyBytes: function () {
    return CryptoJS.lib.WordArray.random(32);
  },
  encrypt: function (aesKeyBytes, ivBytes, plainText) {
    var encryptedBytes = CryptoJS.AES.encrypt(plainText, aesKeyBytes, {
      iv: ivBytes,
      mode: CryptoJS.mode.CBC,
      padding: CryptoJS.pad.Pkcs7,
    });
    return encryptedBytes.toString();
  },
  decrypt: function (aesKeyBytes, ivBytes, cipherTextBase64) {
    var decrypt = CryptoJS.AES.decrypt(cipherTextBase64, aesKeyBytes, {
      iv: ivBytes,
      mode: CryptoJS.mode.CBC,
      padding: CryptoJS.pad.Pkcs7,
    });
    var plaintext = decrypt.toString(CryptoJS.enc.Utf8);
    return plaintext;
  },
};
var RSACryptography = {
  encryptRsa: function (rsaPublicKeyBase64String, plainText) {
    var RSACrypto = new JSEncrypt();
    RSACrypto.setPublicKey(rsaPublicKeyBase64String);
    var encryptedBytes = RSACrypto.encrypt(plainText);
    return encryptedBytes.toString();
  },
};
